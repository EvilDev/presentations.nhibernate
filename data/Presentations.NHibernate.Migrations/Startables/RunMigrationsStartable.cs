﻿using System.Configuration;
using System.Reflection;

using Castle.Core;

using FluentMigrator.Runner;
using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;

using Presentations.NHibernate.Persistence.Constants;

namespace Presentations.NHibernate.Web
{
    public class RunMigrationsStartable : IStartable
    {
        public void Start()
        {
            var connectionString = ConfigurationManager.ConnectionStrings[ConnectionStrings.DefaultConnection].ConnectionString;

            var announcer = new TextWriterAnnouncer(s => System.Diagnostics.Debug.WriteLine(s));
            var assembly = Assembly.GetExecutingAssembly();

            var migrationContext = new RunnerContext(announcer)
            {
                Namespace = "Presentations.NHibernate.Migrations.Migrations"
            };

            var factory = new FluentMigrator.Runner.Processors.SqlServer.SqlServer2012ProcessorFactory();
            var processor = factory.Create(connectionString, announcer, new ProcessorOptions { PreviewOnly = false, Timeout = 60});
            var runner = new MigrationRunner(assembly, migrationContext, processor);
            runner.MigrateUp();
        }

        public void Stop()
        {
        }
    }
}