﻿using FluentMigrator;

namespace Presentations.NHibernate.Migrations.Migrations
{
    [Migration(20130922040236)]
    public class M20130922040236InsertPlayers : Migration
    {
        public override void Down()
        {
            this.Delete.FromTable("Player").Row(new { Name = "Joe" });
            this.Delete.FromTable("Player").Row(new { Name = "Mark" });
            this.Delete.FromTable("Player").Row(new { Name = "Jonathan" });
            this.Delete.FromTable("Player").Row(new { Name = "Bill" });
            this.Delete.FromTable("Player").Row(new { Name = "Cam" });
            this.Delete.FromTable("Player").Row(new { Name = "Ndamukong" });
            this.Delete.FromTable("Player").Row(new { Name = "Steve" });
            this.Delete.FromTable("Player").Row(new { Name = "Frank" });
        }

        public override void Up()
        {
            this.Insert.IntoTable("Player").Row(new { Name = "Joe", BirthDate = "1984-05-12", Height = "72", PositionId = "1", TeamId = "2", Weight = "250" });
            this.Insert.IntoTable("Player").Row(new { Name = "Mark", BirthDate = "1984-05-12", Height = "72", PositionId = "2", TeamId = "1", Weight = "251" });
            this.Insert.IntoTable("Player").Row(new { Name = "Jonathan", BirthDate = "1984-05-12", Height = "72", PositionId = "3", TeamId = "3", Weight = "252" });
            this.Insert.IntoTable("Player").Row(new { Name = "Bill", BirthDate = "1984-05-12", Height = "72", PositionId = "2", TeamId = "5", Weight = "253" });
            this.Insert.IntoTable("Player").Row(new { Name = "Cam", BirthDate = "1984-05-12", Height = "72", PositionId = "1", TeamId = "12", Weight = "254" });
            this.Insert.IntoTable("Player").Row(new { Name = "Ndamukong", BirthDate = "1984-05-12", Height = "72", PositionId = "2", TeamId = "12", Weight = "255" });
            this.Insert.IntoTable("Player").Row(new { Name = "Steve", BirthDate = "1984-05-12", Height = "72", PositionId = "3", TeamId = "4", Weight = "256" });
            this.Insert.IntoTable("Player").Row(new { Name = "Frank", BirthDate = "1984-05-12", Height = "72", PositionId = "2", TeamId = "5", Weight = "257" });
        }
    }
}