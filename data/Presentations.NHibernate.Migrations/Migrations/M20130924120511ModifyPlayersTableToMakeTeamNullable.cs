﻿using FluentMigrator;

namespace Presentations.NHibernate.Migrations.Migrations
{
    [Migration(20130924120511)]
    public class M20130924120511ModifyPlayersTableToMakeTeamNullable : Migration
    {
        public override void Up()
        {
            this.Alter.Table("Player").AlterColumn("TeamId").AsInt32().Nullable();

            this.Insert.IntoTable("Player").Row(new { Name = "Julian", BirthDate = "1984-05-12", Height = "72", PositionId = "1", Weight = "250" });
            this.Insert.IntoTable("Player").Row(new { Name = "Herman", BirthDate = "1984-05-12", Height = "72", PositionId = "2", Weight = "251" });
            this.Insert.IntoTable("Player").Row(new { Name = "Mike", BirthDate = "1984-05-12", Height = "72", PositionId = "3", Weight = "252" });
            this.Insert.IntoTable("Player").Row(new { Name = "Mitch", BirthDate = "1984-05-12", Height = "72", PositionId = "2", Weight = "253" });
            this.Insert.IntoTable("Player").Row(new { Name = "Adam", BirthDate = "1984-05-12", Height = "72", PositionId = "1", Weight = "254" });
            this.Insert.IntoTable("Player").Row(new { Name = "Elron", BirthDate = "1984-05-12", Height = "72", PositionId = "2", Weight = "255" });
            this.Insert.IntoTable("Player").Row(new { Name = "Minator", BirthDate = "1984-05-12", Height = "72", PositionId = "3", Weight = "256" });
            this.Insert.IntoTable("Player").Row(new { Name = "Homer", BirthDate = "1984-05-12", Height = "72", PositionId = "2", Weight = "257" });
        }

        public override void Down()
        {
            this.Alter.Table("Player").AlterColumn("TeamId").AsInt32().NotNullable();
        }
    }
}