﻿using FluentMigrator;

namespace Presentations.NHibernate.Migrations.Migrations
{
    [Migration(1)]
    public class M20130922014908InitializeTables : Migration
    {
        public override void Up()
        {

            this.Create.Table("Conference")
                   .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                   .WithColumn("Name").AsString().NotNullable().Unique();

            this.Create.Table("User")
                  .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                  .WithColumn("Name").AsString().NotNullable().Unique();

            this.Create.Table("Position")
                  .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                  .WithColumn("Name").AsString().NotNullable().Unique();

            this.Create.Table("Team")
                  .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                  .WithColumn("Name").AsString().Unique()
                  .WithColumn("ConferenceId").AsInt32().NotNullable().ForeignKey("Conference", "Id");

            this.Create.Table("Player")
                  .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                  .WithColumn("Name").AsString().NotNullable()
                  .WithColumn("TeamId").AsInt32().NotNullable().ForeignKey("Team", "Id")
                  .WithColumn("BirthDate").AsDate().Nullable()
                  .WithColumn("Height").AsInt32().Nullable()
                  .WithColumn("Weight").AsInt32().Nullable()
                  .WithColumn("PositionId").AsInt32().NotNullable().ForeignKey("Position", "Id");
        }

        public override void Down()
        {
            this.Delete.Table("Player");
            this.Delete.Table("Team");
            this.Delete.Table("Position");
            this.Delete.Table("User");
            this.Delete.Table("Conference");
        }
    }
}