﻿using FluentMigrator;

namespace Presentations.NHibernate.Migrations.Migrations
{
    [Migration(20130922040102)]
    public class M20130922040102InsertPositions : Migration
    {
        public override void Down()
        {
            this.Delete.FromTable("Position").Row(new { Name = "Quarterback" });
            this.Delete.FromTable("Position").Row(new { Name = "Running Back" });
            this.Delete.FromTable("Position").Row(new { Name = "Tight End" });
            this.Delete.FromTable("Position").Row(new { Name = "Kicker" });
            this.Delete.FromTable("Position").Row(new { Name = "Offensive Lineman" });
        }

        public override void Up()
        {
            this.Insert.IntoTable("Position").Row(new { Name = "Quarterback"});
            this.Insert.IntoTable("Position").Row(new { Name = "Running Back"});
            this.Insert.IntoTable("Position").Row(new { Name = "Tight End"});
            this.Insert.IntoTable("Position").Row(new { Name = "Kicker"});
            this.Insert.IntoTable("Position").Row(new { Name = "Offensive Lineman"});
        }
    }
}