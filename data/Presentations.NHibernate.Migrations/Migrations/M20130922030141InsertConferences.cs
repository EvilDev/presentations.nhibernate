﻿using FluentMigrator;

namespace Presentations.NHibernate.Migrations.Migrations
{
    [Migration(20130922030141)]
    public class M20130922030141InsertConferences : Migration
    {
        public override void Down()
        {
            this.Delete.FromTable("Conference").Row(new { Name = "American Football Conference" });
            this.Delete.FromTable("Conference").Row(new { Name = "National Football Conference" });
        }

        public override void Up()
        {
            this.Insert.IntoTable("Conference").Row(new { Name = "American Football Conference"});
            this.Insert.IntoTable("Conference").Row(new { Name = "National Football Conference"});
        }
    }
}