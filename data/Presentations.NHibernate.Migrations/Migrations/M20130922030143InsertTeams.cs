﻿using FluentMigrator;

namespace Presentations.NHibernate.Migrations.Migrations
{
    [Migration(20130922030143)]
    public class M20130922030143InsertTeams : Migration
    {
        public override void Down()
        {
            this.Delete.FromTable("Team").Row(new { Name = "Buffalo Bills" });
            this.Delete.FromTable("Team").Row(new { Name = "Miami Dolphins" });
            this.Delete.FromTable("Team").Row(new { Name = "New England Patriots" });
            this.Delete.FromTable("Team").Row(new { Name = "New York Jets" });
            this.Delete.FromTable("Team").Row(new { Name = "Baltimore Ravens" });
            this.Delete.FromTable("Team").Row(new { Name = "Cincinnati Bengals" });
            this.Delete.FromTable("Team").Row(new { Name = "Cleveland Browns" });
            this.Delete.FromTable("Team").Row(new { Name = "Pittsburgh Steelers" });
            this.Delete.FromTable("Team").Row(new { Name = "Houston Texans" });
            this.Delete.FromTable("Team").Row(new { Name = "Indianapolis Colts" });
            this.Delete.FromTable("Team").Row(new { Name = "Jacksonville Jaguars" });
            this.Delete.FromTable("Team").Row(new { Name = "Tennessee Titans" });
            this.Delete.FromTable("Team").Row(new { Name = "Denver Broncos" });
            this.Delete.FromTable("Team").Row(new { Name = "Kansas City Chiefs" });
            this.Delete.FromTable("Team").Row(new { Name = "Oakland Raiders" });
            this.Delete.FromTable("Team").Row(new { Name = "San Diego Chargers" });
            this.Delete.FromTable("Team").Row(new { Name = "Dallas Cowboys" });
            this.Delete.FromTable("Team").Row(new { Name = "New York Giants" });
            this.Delete.FromTable("Team").Row(new { Name = "Philadelphia Eagles" });
            this.Delete.FromTable("Team").Row(new { Name = "Washington Redskins" });
            this.Delete.FromTable("Team").Row(new { Name = "Chicago Bears" });
            this.Delete.FromTable("Team").Row(new { Name = "Detroit Lions" });
            this.Delete.FromTable("Team").Row(new { Name = "Green Bay Packers" });
            this.Delete.FromTable("Team").Row(new { Name = "Minnesota Vikings" });
            this.Delete.FromTable("Team").Row(new { Name = "Atlanta Falcons" });
            this.Delete.FromTable("Team").Row(new { Name = "Carolina Panthers" });
            this.Delete.FromTable("Team").Row(new { Name = "New Orleans Saints" });
            this.Delete.FromTable("Team").Row(new { Name = "Tampa Bay Buccaneers" });
            this.Delete.FromTable("Team").Row(new { Name = "Arizona Cardinals" });
            this.Delete.FromTable("Team").Row(new { Name = "St. Louis Rams" });
            this.Delete.FromTable("Team").Row(new { Name = "San Francisco 49ers" });
            this.Delete.FromTable("Team").Row(new { Name = "Seattle Seahawks" });
        }

        public override void Up()
        {
            this.Insert.IntoTable("Team").Row(new { Name = "Buffalo Bills", ConferenceId = 1});
            this.Insert.IntoTable("Team").Row(new { Name = "Miami Dolphins", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "New England Patriots", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "New York Jets", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Baltimore Ravens", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Cincinnati Bengals", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Cleveland Browns", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Pittsburgh Steelers", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Houston Texans", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Indianapolis Colts", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Jacksonville Jaguars", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Tennessee Titans", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Denver Broncos", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Kansas City Chiefs", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Oakland Raiders", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "San Diego Chargers", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Dallas Cowboys", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "New York Giants", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Philadelphia Eagles", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Washington Redskins", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Chicago Bears", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Detroit Lions", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Green Bay Packers", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Minnesota Vikings", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Atlanta Falcons", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Carolina Panthers", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "New Orleans Saints", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Tampa Bay Buccaneers", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Arizona Cardinals", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "St. Louis Rams", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "San Francisco 49ers", ConferenceId = 1 });
            this.Insert.IntoTable("Team").Row(new { Name = "Seattle Seahawks", ConferenceId = 1 });
        }
    }
}