﻿using System;

namespace Presentations.NHibernate.Persistence.Common
{
	public class DomainEvent
	{
		public int Id { get; set; }
		public DateTime EventDate { get; set; }
	}
}
