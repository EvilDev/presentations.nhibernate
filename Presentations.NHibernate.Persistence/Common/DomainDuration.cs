﻿using System;

namespace Presentations.NHibernate.Persistence.Common
{
	public class DomainDuration
	{
		public int Id { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
	}
}
