﻿namespace Presentations.NHibernate.Persistence.Common
{
	public class DomainEntity
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
