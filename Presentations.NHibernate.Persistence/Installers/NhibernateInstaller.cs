﻿using System.Configuration;
using System.Diagnostics;
using System.Reflection;

using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

using NHibernate;
using NHibernate.SqlCommand;
using NHibernate.Tool.hbm2ddl;

using Presentations.NHibernate.Persistence.Constants;

namespace Presentations.NHibernate.Persistence.Installers
{
    public class NhibernateInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var config = Fluently.Configure()
                     .Database(MsSqlConfiguration.MsSql2008.ShowSql().ConnectionString(ConfigurationManager.ConnectionStrings[ConnectionStrings.DefaultConnection].ConnectionString))
					 .ExposeConfiguration(x => x.SetInterceptor(new SqlStatementInterceptor()))
                     .Mappings(m => m.AutoMappings.Add(
                         AutoMap.Assembly(Assembly.GetExecutingAssembly())
                                .Where(t => t.Namespace.EndsWith("Models"))
                                .Conventions
                                .AddAssembly(Assembly.GetExecutingAssembly()) // Change to KORE.Commons.Conventions
                                .UseOverridesFromAssembly(Assembly.GetExecutingAssembly())))
                     .BuildConfiguration();

            container.Register(Component.For<ISessionFactory>().UsingFactoryMethod(config.BuildSessionFactory));
        }
    }

	public class SqlStatementInterceptor : EmptyInterceptor
	{
		public override SqlString OnPrepareStatement(SqlString sql)
		{
			Trace.WriteLine(sql.ToString());
			return sql;
		}
	}
}