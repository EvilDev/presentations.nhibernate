﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

using Presentations.NHibernate.Persistence.Models;

namespace Presentations.NHibernate.Persistence.Overrides
{
    public class TeamOverride : IAutoMappingOverride<Team>
    {
        public void Override(AutoMapping<Team> mapping)
        {
            mapping.References(x => x.Conference);
        }
    }
}