﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

using Presentations.NHibernate.Persistence.Models;

namespace Presentations.NHibernate.Persistence.Overrides
{
    public class PlayerOverrides : IAutoMappingOverride<Player>
    {
        public void Override(AutoMapping<Player> mapping)
        {
            mapping.References(x => x.Team).Column("TeamId");
        }
    }
}