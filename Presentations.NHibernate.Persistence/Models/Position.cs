﻿namespace Presentations.NHibernate.Persistence.Models
{
    public class Position
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }
    }
}