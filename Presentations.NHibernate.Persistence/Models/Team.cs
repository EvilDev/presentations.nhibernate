﻿namespace Presentations.NHibernate.Persistence.Models
{
    public class Team
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual Conference Conference { get; set; }
    }
}