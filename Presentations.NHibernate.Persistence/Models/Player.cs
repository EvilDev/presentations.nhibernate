﻿using System;

namespace Presentations.NHibernate.Persistence.Models
{
    public class Player
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual Team Team { get; set; }

        public virtual DateTime BirthDate { get; set; }

        public virtual int Height { get; set; }

        public virtual Position Position { get; set; }

        public virtual int Weight { get; set; }
    }
}