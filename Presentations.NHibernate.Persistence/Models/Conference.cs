﻿namespace Presentations.NHibernate.Persistence.Models
{
    public class Conference
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }
    }
}