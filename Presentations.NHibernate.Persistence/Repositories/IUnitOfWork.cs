﻿using System;
using System.Linq;

namespace Presentations.NHibernate.Persistence.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        void Add<T>(T entity);

        IQueryable<T> Query<T>();

        void Remove<T>(T entity);

        void Update<T>(T entity);
    }
}