﻿using System.Collections.Generic;
using System.Linq;

namespace Presentations.NHibernate.Persistence.Repositories
{
    public interface IRepository<T>
    {
        void Add(T entity);

        void Remove(T entity);

        IQueryable<T> Query();

        void Update(T entity);
    }
}