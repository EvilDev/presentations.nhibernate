﻿using Presentations.NHibernate.Persistence.Models;

namespace Presentations.NHibernate.Persistence.Repositories
{
    public interface IConferenceRepository : IRepository<Conference>
    {
    }
}