﻿using System.Linq;

using Presentations.NHibernate.Persistence.Models;

namespace Presentations.NHibernate.Persistence.Repositories
{
    public class TeamRepository : BaseRepository<Team>, ITeamRepository
    {
        public TeamRepository(IUnitOfWorkFactory factory)
            : base(factory)
        {
        }

        public Team GetById(int id)
        {
            return this.Query().FirstOrDefault(t => t.Id == id);
        }
    }
}