﻿using Presentations.NHibernate.Persistence.Models;

namespace Presentations.NHibernate.Persistence.Repositories
{
    public interface ITeamRepository : IRepository<Team>
    {
        Team GetById(int id);
    }
}