﻿namespace Presentations.NHibernate.Persistence.Repositories
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork Create();

        void Release(IUnitOfWork uow);
    }
}