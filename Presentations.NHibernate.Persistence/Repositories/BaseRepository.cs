﻿using System.Linq;

namespace Presentations.NHibernate.Persistence.Repositories
{
    public abstract class BaseRepository<T> : IRepository<T>
    {
        private readonly IUnitOfWorkFactory _factory;

        protected BaseRepository(IUnitOfWorkFactory factory)
        {
            _factory = factory;
        }

        public void Add(T entity)
        {
            _factory.Create().Add(entity);
        }

        public void Remove(T entity)
        {
            _factory.Create().Remove(entity);
        }

        public IQueryable<T> Query()
        {
            return _factory.Create().Query<T>();
        }

        public void Update(T entity)
        {
            _factory.Create().Update(entity);
        }
    }
}