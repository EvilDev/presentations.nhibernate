﻿using System.Collections.Generic;
using System.Linq;

using Presentations.NHibernate.Persistence.Models;

namespace Presentations.NHibernate.Persistence.Repositories
{
    public interface IPlayerRepository : IRepository<Player>
    {
        IEnumerable<Player> GetUndraftedPlayers();

        IEnumerable<Player> GetPlayersOnTeam(Team team);
    }

    public class PlayerRepository : BaseRepository<Player>, IPlayerRepository
    {
        public PlayerRepository(IUnitOfWorkFactory factory)
            : base(factory)
        {
        }

        public IEnumerable<Player> GetUndraftedPlayers()
        {
            return Query().Where(p => p.Team == null);
        }

        public IEnumerable<Player> GetPlayersOnTeam(Team team)
        {
            return this.Query().Where(p => p.Team == team);
        }
    }
}