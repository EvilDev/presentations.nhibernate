﻿using Presentations.NHibernate.Persistence.Models;

namespace Presentations.NHibernate.Persistence.Repositories
{
    public class ConferenceRepository : BaseRepository<Conference>, IConferenceRepository
    {
        public ConferenceRepository(IUnitOfWorkFactory factory)
            : base(factory)
        {
        }
    }
}