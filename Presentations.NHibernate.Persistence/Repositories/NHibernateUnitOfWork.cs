﻿using System.Linq;

using NHibernate;
using NHibernate.Linq;

namespace Presentations.NHibernate.Persistence.Repositories
{
    public class NHibernateUnitOfWork : IUnitOfWork
    {
        private readonly ISessionFactory _sessionFactory;

        private ISession _session;

        private ITransaction _transaction;

        public NHibernateUnitOfWork(ISessionFactory sessionFactory)
        {
            this._sessionFactory = sessionFactory;
        }

        public void Add<T>(T entity)
        {
            this.Initialize();
            this._session.Save(entity);
        }

        public void Dispose()
        {
            if (this._transaction != null)
            {
                try
                {
                    this._transaction.Commit();
                    this._transaction.Dispose();
                }
                catch
                {
                    this._transaction.Rollback();
                    throw;
                }
            }

            if (this._session != null)
            {
                this._session.Dispose();
            }
        }

        public IQueryable<T> Query<T>()
        {
            this.Initialize();
            return this._session.Query<T>();
        }

        public void Remove<T>(T entity)
        {
            this.Initialize();
            this._session.Delete(entity);
        }

        public void Update<T>(T entity)
        {
            this.Initialize();
            this._session.Update(entity);
        }

        private void Initialize()
        {
            if (this._session == null)
            {
                this._session = this._sessionFactory.OpenSession();
                this._transaction = this._session.BeginTransaction();
            }
        }
    }
}