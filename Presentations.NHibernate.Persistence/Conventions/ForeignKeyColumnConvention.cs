﻿using System;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace Presentations.NHibernate.Persistence.Conventions
{
	public class ForeignKeyColumnConvention : IReferenceConvention
	{
		public void Apply(IManyToOneInstance instance)
		{
			instance.Column(instance.Name + "Id");
		}
	}
}
