﻿using System.Web.Mvc;

using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using IDependencyResolver = System.Web.Mvc.IDependencyResolver;

namespace Presentations.NHibernate.Web.Installers
{
    public class PlumbingInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IKernel>().Instance(container.Kernel));

            container.Register(Component.For<IDependencyResolver>().ImplementedBy<WindsorDependencyResolver>());

            container.Register(Component.For<IControllerFactory>().ImplementedBy<WindsorControllerFactory>());
        }
    }
}