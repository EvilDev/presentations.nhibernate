﻿using System.Web;

using Castle.Facilities.Startable;
using Castle.Facilities.TypedFactory;
using Castle.Windsor;
using Castle.Windsor.Installer;

using Presentations.NHibernate.Migrations.Installers;

namespace Presentations.NHibernate.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class FantasyFootballApplication : HttpApplication
    {
        private readonly IWindsorContainer _container;

        public FantasyFootballApplication()
        {
            _container = new WindsorContainer();
        }

        protected void Application_Start()
        {
            _container.AddFacility<StartableFacility>();
            _container.AddFacility<TypedFactoryFacility>();

            _container.Install(FromAssembly.InThisApplication());
            _container.Install(new StartablesInstaller());
        }

        protected void Application_End()
        {
            _container.Dispose();
        }
    }
}