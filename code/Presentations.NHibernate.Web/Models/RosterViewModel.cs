﻿using System.Collections.Generic;

using Presentations.NHibernate.Persistence.Models;

namespace Presentations.NHibernate.Web.Models
{
    public class RosterViewModel
    {
        public Team Team { get; set; }

        public IEnumerable<Player> CurrentPlayers { get; set; }

        public IEnumerable<Player> AvailablePlayers { get; set; }
    }
}