﻿using System;
using System.Collections.Generic;
using System.Linq;

using Castle.MicroKernel;

using IDependencyResolver = System.Web.Mvc.IDependencyResolver;

namespace Presentations.NHibernate.Web
{
    public class WindsorDependencyResolver : IDependencyResolver
    {
        private readonly IKernel _container;

        public WindsorDependencyResolver(IKernel container)
        {
            _container = container;
        }

        public object GetService(Type serviceType)
        {
            return _container.Resolve(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _container.ResolveAll(serviceType).Cast<object>();
        }
    }
}