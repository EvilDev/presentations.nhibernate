﻿using System.Web.Mvc;

using Castle.Core;

namespace Presentations.NHibernate.Web
{
    public class FilterConfig : IStartable
    {
        private readonly GlobalFilterCollection _filters;

        public FilterConfig(GlobalFilterCollection filters)
        {
            _filters = filters;
        }

        public void Start()
        {
            _filters.Add(new HandleErrorAttribute());
        }

        public void Stop()
        {
        }
    }
}