﻿using System.Web.Mvc;
using System.Web.Routing;

using Castle.Core;

namespace Presentations.NHibernate.Web
{
    public class RouteConfig : IStartable
    {
        private readonly RouteCollection _routes;

        public RouteConfig(RouteCollection routes)
        {
            _routes = routes;
        }

        public void Start()
        {
            _routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            _routes.MapRoute(name: "Default", url: "{controller}/{action}/{id}", defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });
        }

        public void Stop()
        {
        }
    }
}