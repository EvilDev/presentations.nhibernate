﻿using System.Web.Mvc;

using Castle.Core;

namespace Presentations.NHibernate.Web
{
    public class WireupMvcStartable : IStartable
    {
        private readonly IControllerFactory _controllerFactory;

        public WireupMvcStartable(IControllerFactory controllerFactory)
        {
            _controllerFactory = controllerFactory;
        }

        public void Start()
        {
            ControllerBuilder.Current.SetControllerFactory(_controllerFactory);
        }

        public void Stop()
        {
        }
    }
}