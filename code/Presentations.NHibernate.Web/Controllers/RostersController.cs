﻿using System.Linq;
using System.Web.Mvc;

using Presentations.NHibernate.Persistence.Models;
using Presentations.NHibernate.Persistence.Repositories;
using Presentations.NHibernate.Web.Models;

namespace Presentations.NHibernate.Web.Controllers
{
    public class RostersController : Controller
    {
        private readonly IPlayerRepository _playerRepo;

        private readonly ITeamRepository _teamRepo;

        public RostersController(IPlayerRepository playerRepo, ITeamRepository teamRepo)
        {
            _playerRepo = playerRepo;
            _teamRepo = teamRepo;
        }

        public ActionResult Index(Team team)
        {
            if (string.IsNullOrEmpty(team.Name))
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Title = string.Format("Showing Roster for {0}", team.Name);

            return View(new RosterViewModel { Team = team, CurrentPlayers = _playerRepo.GetPlayersOnTeam(team), AvailablePlayers = _playerRepo.GetUndraftedPlayers() });
        }

        public ActionResult Add(int teamId, int playerId)
        {
            var player = _playerRepo.Query().FirstOrDefault(p => p.Id == playerId);
            var team = _teamRepo.GetById(teamId);

            if (player != null)
            {
                player.Team = team;
                _playerRepo.Update(player);
            }
            return RedirectToAction("Index","Rosters", team);
        }

        public ActionResult Remove(int teamId, int playerId)
        {
            var player = _playerRepo.Query().FirstOrDefault(p => p.Id == playerId);
            var team = _teamRepo.GetById(teamId);

            if (player != null)
            {
                player.Team = null;

                this._playerRepo.Update(player);
            }

            return RedirectToAction("Index", "Rosters", team);
        }
    }
}