﻿using System.Linq;
using System.Web.Mvc;

using Presentations.NHibernate.Persistence.Models;
using Presentations.NHibernate.Persistence.Repositories;

namespace Presentations.NHibernate.Web.Controllers
{
    public class TeamsController : Controller
    {
        private readonly ITeamRepository _fantasyTeams;

        private readonly IConferenceRepository _conferences;

        public TeamsController(ITeamRepository fantasyTeams, IConferenceRepository conferences)
        {
            _fantasyTeams = fantasyTeams;
            _conferences = conferences;
        }

        public ActionResult Create()
        {
            ViewBag.Title = "Create new team";

            ViewBag.Conferences = new SelectList(_conferences.Query().AsEnumerable(), "Id", "Name");

            var team = new Team();

            return View(team);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Delete(Team team)
        {
            _fantasyTeams.Remove(team);
            return RedirectToAction("Index", new { Message = "Removed Successfully." });
        }

        public ActionResult Edit(Team team)
        {
            ViewBag.Title = string.Format("Modify {0} Team", team.Name);
            ViewBag.Conferences = new SelectList(_conferences.Query().AsEnumerable(), "Conference", "Name");
            return View(team);
        }

        public ActionResult Index(string message)
        {
            var teams = _fantasyTeams.Query();

            ViewBag.Title = "Teams";
            ViewBag.Message = message;
            return View(teams);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Save(Team team)
        {
            if (team.Id == 0)
            {
                _fantasyTeams.Add(team);
            }
            else
            {
                _fantasyTeams.Update(team);
            }

            return RedirectToAction("Index", new { Message = "Saved Successfully." });
        }

        public ActionResult Top(int count)
        {
            var teams = _fantasyTeams.Query().AsEnumerable().Take(count);
            return PartialView("_List", teams);
        }
    }
}